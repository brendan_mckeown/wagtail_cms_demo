# Add author to blog posts

## Create a new snippet class `BlogAuthor` in `blog/models.py`

Snippets are simpler content models that do not need their own page.

```
@register_snippet
class BlogAuthor(models.Model):
    name = models.CharField(max_length=255)
    avatar = models.ForeignKey(
        "wagtailimages.Image", null=True, blank=True,
        on_delete=models.SET_NULL, related_name="+"
    )

    panels = [
        FieldPanel("name"),
        ImageChooserPanel("avatar"),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "blog authors"
```
## Add `author` field to `BlogPage` page class

This will define a one-to-many relationship (i.e. one author can have many blog posts).

```
    author = models.ForeignKey(
        "blog.BlogAuthor",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+"
    )

    FieldPanel("author"),
```
## Run database migrations
```
python manage.py makemigrations
python manage.py migrate
```
## Add author to `blog_index_page.html` and `blog_page.html` templates

```
{% if post.author %}
    by {{ post.author }}
{% endif %}

{% if page.author %}
    <figure>
        {% image page.author.avatar fill-90x90 style="vertical-align: middle" %}
        <caption>{{ page.author.name }}</caption>
    </figure>
{% endif %}
```
## Start web server
Create blog authors and assign authors to blog posts

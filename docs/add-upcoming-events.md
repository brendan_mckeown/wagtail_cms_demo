# Add upcoming events

## Create a new app for `events`
This command scaffolds the files necessary for a new app in your site.
```
python manage.py startapp events
```

## Add app to installed apps in `mysite/settings/base.py`
Add the new `events` app to `INSTALLED_APPS` array


## Define page models in `events/models.py`
```
from django.db import models
from django.utils import timezone

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.search import index


class EventIndexPage(Page):
    intro = models.CharField(max_length=250)

    content_panels = Page.content_panels + [
        FieldPanel("intro")
    ]

    def get_context(self, request):
        context = super().get_context(request)
        events = EventPage.get_upcoming_events()
        context["upcoming_events"] = events
        return context


class EventPage(Page):
    date = models.DateField("Event date")
    start_time = models.TimeField("Event time")
    location = models.CharField(max_length=250)
    address = models.TextField(max_length=250)
    body = RichTextField(blank=True)

    search_fields = Page.search_fields + [
        index.SearchField("location"),
        index.SearchField("address"),
        index.SearchField("body"),
    ]

    content_panels = Page.content_panels + [
        FieldPanel("date"),
        FieldPanel("start_time"),
        FieldPanel("location"),
        FieldPanel("address"),
        FieldPanel("body", classname="full"),
    ]

    @staticmethod
    def get_upcoming_events():
        now = timezone.now()
        return EventPage.objects.all().live().filter(date__gte=now).order_by("date")

```

## Create page templates

Create file `events/templates/events/event_index_page.html`

```
{% extends "base.html" %}

{% load wagtailcore_tags %}

{% block content %}

    <div class="tc-hero">
        <h1 class="tc-hero-title">{{ page.title }}</h1>
        <p class="tc-hero-subtitle">{{ page.intro }}</p>
    </div>

    <div class="tc-layout--home tc-container">
        <section class="tc-posts-section">
            {% for event in upcoming_events %}
                {% with event=event.specific %}
                    <article class="tc-post">
                        <a class="tc-post-link" href="{% pageurl event %}">
                            {{ event.title }}
                        </a>
                        <p>{{ event.date }} {{ event.start_time }}</p>
                    </article>
                {% endwith %}
            {% endfor %}
        </section>
    </div>

{% endblock %}
```
Create file `events/templates/events/event_page.html`
```
{% extends "base.html" %}

{% load wagtailcore_tags %}

{% block header_class %}tc-header--static{% endblock %}

{% block content %}

    <div class="tc-layout--post">
        <div class="tc-container">
            <article class="tc-post">
                <h1 class="tc-post-title">{{ page.title }}</h1>
                <cite class="tc-post-author">{{ page.date }} {{ page.start_time }}</cite>
                <p>{{ page.location }}</p>
                <p>{{ page.address }}</p>
                <div class="tc-post-body">
                    {{ page.body|richtext }}
                </div>
            </article>

            <ul class="tc-post-navigation-links">
                <li>
                    <a class="tc-post-navigation-link" rel="next" href="{{ page.get_parent.url }}">
                        ← Return to events
                    </a>
                </li>
            </ul>
        </div>
    </div>

{% endblock %}
```

## Run migrations
```
python manage.py makemigrations
python manage.py migrate
```

## Add events list to context of blog index page model
```
from events.models import EventPage

events = EventPage.get_upcoming_events()
context["upcoming_events"] = events
```

## Add events list to blog index page template

```
<ul class="tc-event-list">
    {% for event in upcoming_events %}
    {% with event=event.specific %}
    <li>
        <h4 class="tc-event-title">
            <a href="{% pageurl event %}">{{ event.title }}</a>
        </h4>
        <time class="tc-event-date">{{ event.date }}, {{ event.start_time }}</time>
        <span class="tc-event-location">{{ event.location }}</span>
        <span class="tc-event-location">{{ event.address }}</span>
    </li>
    {% endwith %}
    {% endfor %}
</ul>
```

## Create event index page and events in admin
Create a new child page of the homepage for the events index page. Then create child pages under the events index page for individual events.


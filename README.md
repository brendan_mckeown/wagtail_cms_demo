# Wagtail CMS Bake-off Demo

## Prerequisites

- Docker
    - [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
    - [Docker for Windows](https://docs.docker.com/docker-for-windows/install/)

## Setup

```
# build image and start container in the background
docker-compose up -d

# open shell in the web container
docker exec -it wagtail-demo_web_1 /bin/bash

# inside the shell, navigate to site directory
cd mysite

# create the database
python manage.py migrate

# create a wagtail admin
python manage.py createsuperuser

# start a web server at http://localhost:8000
python manage.py runserver 0.0.0.0:8000
```

## Development

```
# to see all available commands
python manage.py

# after making any model changes, run:
python manage.py makemigrations
python manage.py migrate

# create a new app named "blog"
python manage.py startapp blog

# exit shell
exit

# show all running containers
docker container ls

# stop a running container
docker container stop [container ID]
```

## Resources

- [Wagtail docs](http://docs.wagtail.io/)
- [Django docs](https://docs.djangoproject.com/en/2.1/)
- [Python docs](https://docs.python.org/3/)
